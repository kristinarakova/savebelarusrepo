import UIKit

class RecordsTableViewCell: UITableViewCell {

    @IBOutlet weak var playerNameFlagsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
}
