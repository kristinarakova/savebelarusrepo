import UIKit
import AVFoundation

class StartViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var infoContainerLabel: UIView!
    @IBOutlet weak var buttonsContainerLabel: UIView!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    @IBOutlet weak var nameGameLabel: UILabel!
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var recordsButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addingMusic()
        self.addBackgroundImage()
        self.creatingButtonsUI()
    }
    
    @IBAction func playButton(_ sender: UIButton) {
        
        guard let playgroundController = storyboard?.instantiateViewController(withIdentifier: "PlaygroundViewController") as? PlaygroundViewController else {return}
        self.navigationController?.pushViewController(playgroundController, animated: true)
    }
    
    @IBAction func recordsButton(_ sender: UIButton) {
        
        guard let recordsController = storyboard?.instantiateViewController(withIdentifier: "RecordsViewController") as? RecordsViewController else {return}
        self.navigationController?.pushViewController(recordsController, animated: true)
    }
    
    @IBAction func settingsButton(_ sender: UIButton) {
        
        guard let settingsController = storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else {return}
        self.navigationController?.pushViewController(settingsController, animated: true)
    }
    
    private func addingMusic () {
        
        let sound = Manager.shared.loadMusicPlayer()
        if sound == true {
            Manager.shared.playAudio()
        } else if sound == false {
            Manager.shared.stopAudio()
        }
    }
    
    private func addBackgroundImage () {
        self.backgroundImageView.image = UIImage(named: "startBackground")
    }
    
    private func creatingButtonsUI () {
        
        self.playButton.addGradientThreeColors(firstColor: .white, secondColor: .red, thirdColor: .white, opacity: 0.9, startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5), radius: self.playButton.frame.height/2)
        self.recordsButton.addGradientThreeColors(firstColor: .white, secondColor: .red, thirdColor: .white, opacity: 0.9, startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5), radius: self.recordsButton.frame.height/2)
        self.settingsButton.addGradientThreeColors(firstColor: .white, secondColor: .red, thirdColor: .white, opacity: 0.9, startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5), radius: self.settingsButton.frame.height/2)
    }
}
