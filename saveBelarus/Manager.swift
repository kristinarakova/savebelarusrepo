import Foundation
import UIKit
import AVFoundation

class Manager {
    
    static let shared = Manager()
    private init () {}
    
    var audioPlayer: AVAudioPlayer?
    
    func playAudio() {
        
        let sound = URL(fileURLWithPath: Bundle.main.path(forResource: "backgroundMusic", ofType: "mp3")!)
        
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
        try! AVAudioSession.sharedInstance().setActive(true)
        
        try! audioPlayer = AVAudioPlayer(contentsOf: sound)
        audioPlayer!.prepareToPlay()
        audioPlayer!.numberOfLoops = -1
        audioPlayer!.play()
    }
    
    func stopAudio() {
        if let player = self.audioPlayer {
            player.stop()
        }
    }
    
    func saveMusicPlayer (sound: Bool) {
        UserDefaults.standard.set(sound, forKey: "sound")
    }
    
    func loadMusicPlayer () -> Bool {
        guard let sound = UserDefaults.standard.value(forKey: "sound") as? Bool else {return true}
        return sound
    }
    
    func saveName (name: String) {
        UserDefaults.standard.set(name, forKey: "name")
    }
    
    func loadName () -> String? {
        
        guard let name = UserDefaults.standard.value(forKey: "name") as? String else {return "Player"}
        return name
    }
    
    func saveCharacter (character: Int) {
        UserDefaults.standard.set(character, forKey: "character")
    }
    
    func loadCharacter () -> Int? {
        guard let character = UserDefaults.standard.value(forKey: "character") as? Int else {return 0}
        return character
    }
    
    func save(object: PlayerClass) {
        
        var array = Manager.shared.load()
        array.append(object)
        UserDefaults.standard.set(encodable: array, forKey: "game")
    }
    
    func saveEmptyArrayOfResults () {
        
        var array = Manager.shared.load()
        array.removeAll()
        UserDefaults.standard.set(encodable: array, forKey: "game")
    }
    
    func load() -> [PlayerClass] {
        
        let array = UserDefaults.standard.value([PlayerClass].self, forKey: "game")
        
        if let array = array {
            return array
        } else {
            return []
        }
    }
}
