import UIKit

class GameOverViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var resultContainerView: UIView!
    @IBOutlet weak var buttonsContainerView: UIView!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    @IBOutlet weak var gameOverLabel: UILabel!
    @IBOutlet weak var flagsLabel: UILabel!
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var recordsButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.creatingButtonsUI()
        self.creatingLabelsUI()
    }
    
    private func creatingLabelsUI () {
        
        let arrayResults = Manager.shared.load()
        
        self.flagsLabel.addGradientThreeColors(firstColor: .white, secondColor: .red, thirdColor: .white, opacity: 0.9, startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5), radius: self.flagsLabel.frame.height/2)
        self.playerNameLabel.addGradientThreeColors(firstColor: .white, secondColor: .red, thirdColor: .white, opacity: 0.9, startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5), radius: self.playerNameLabel.frame.height/2)
        self.dateLabel.addGradientThreeColors(firstColor: .white, secondColor: .red, thirdColor: .white, opacity: 0.9, startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5), radius: self.dateLabel.frame.height/2)
        
        let flagsLabelText = UILabel()
        flagsLabelText.frame = self.flagsLabel.frame
        if let flags = arrayResults.last?.flags {
            flagsLabelText.text = "\(flags) flags"
        }
        flagsLabelText.textColor = .white
        flagsLabelText.textAlignment = .center
        self.resultContainerView.addSubview(flagsLabelText)
        
        let playerNameLabelText = UILabel()
        playerNameLabelText.frame = self.playerNameLabel.frame
        if let name = arrayResults.last?.name {
            playerNameLabelText.text = name
        }
        playerNameLabelText.textColor = .white
        playerNameLabelText.textAlignment = .center
        self.resultContainerView.addSubview(playerNameLabelText)
        
        let dateLabelText = UILabel()
        dateLabelText.frame = self.dateLabel.frame
        if let date = arrayResults.last?.data {
            dateLabelText.text = date
        }
        dateLabelText.textColor = .white
        dateLabelText.textAlignment = .center
        self.resultContainerView.addSubview(dateLabelText)
    }
    
    private func creatingButtonsUI () {
        
        self.startButton.addGradientThreeColors(firstColor: .white, secondColor: .red, thirdColor: .white, opacity: 0.9, startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5), radius: self.startButton.frame.height/2)
        self.recordsButton.addGradientThreeColors(firstColor: .white, secondColor: .red, thirdColor: .white, opacity: 0.9, startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5), radius: self.recordsButton.frame.height/2)
        self.settingsButton.addGradientThreeColors(firstColor: .white, secondColor: .red, thirdColor: .white, opacity: 0.9, startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5), radius: self.settingsButton.frame.height/2)
    }
    
    @IBAction func startButton(_ sender: UIButton) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func recordsButton(_ sender: UIButton) {
        
        guard let recordsController = storyboard?.instantiateViewController(withIdentifier: "RecordsViewController") as? RecordsViewController else {return}
        self.navigationController?.pushViewController(recordsController, animated: true)
    }
    
    @IBAction func settingsButton(_ sender: UIButton) {
        
        guard let settingsController = storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else {return}
        self.navigationController?.pushViewController(settingsController, animated: true)
    }
}
