import UIKit

class RecordsViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableContainerView: UIView!
    @IBOutlet weak var infoContainerView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var nameGameLabel: UILabel!
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var resetResultsButton: UIButton!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var arrayResult = [PlayerClass]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadResults()
        self.creatingButtonsUI()
    }
    
    private func loadResults () {
        
        let arrayResult = Manager.shared.load()
        self.arrayResult = arrayResult
        self.tableView.reloadData()
    }
    
    private func creatingButtonsUI () {
        
        self.startButton.addGradientThreeColors(firstColor: .white, secondColor: .red, thirdColor: .white, opacity: 0.7, startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5), radius: self.startButton.frame.height/2)
        self.settingsButton.addGradientThreeColors(firstColor: .white, secondColor: .red, thirdColor: .white, opacity: 0.7, startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5), radius: self.settingsButton.frame.height/2)
    }
    
    @IBAction func resetResultsButton(_ sender: UIButton) {
        
        self.resetResultsAlert(title: "Are you sure you want to reset the results?", message: "The action is irreversible", preferredStyle: .alert, titleFirstButton: "Reset", styleFirstButton: .default, handlerFirst: { (_) in
            
            Manager.shared.saveEmptyArrayOfResults()
            self.loadResults()
            
        }, titleSecondButton: "Cancel", styleSecondButton: .default, handlerSecond: nil)
    }
    
    @IBAction func startButton(_ sender: UIButton) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func settingsButton(_ sender: UIButton) {
        
        guard let settingsController = storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else {return}
        self.navigationController?.pushViewController(settingsController, animated: true)
    }
}

extension RecordsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecordsTableViewCell", for: indexPath) as? RecordsTableViewCell else {
            return UITableViewCell()
        }
        
        if let playerName = arrayResult[indexPath.row].name,
           let flags = arrayResult[indexPath.row].flags,
           let date = arrayResult[indexPath.row].data {
            
            cell.playerNameFlagsLabel.text = "\(playerName) collect \(flags) flags"
            cell.dateLabel.text = date
        }
        
        return cell
    }
}
