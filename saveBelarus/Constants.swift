import Foundation
import UIKit

class Constants {
    
    static let charactersArray = [UIImage(named: "firstCharacterSettings"), UIImage(named: "secondCharacterSettings"), UIImage(named: "thirdCharacterSettings"), UIImage(named: "fourthCharacterSettings")]
    
    static let firstCharacter = ["firstCharacterFirstAction", "firstCharacterSecondAction", "firstCharacterThirdAction", "firstCharacterFourthAction", "firstCharacterFifthAction", "firstCharacterSixthAction", "firstCharacterSeventhAction", "firstCharacterEighthAction"]
    static let secondCharacter = ["secondCharacterFirstAction", "secondCharacterSecondAction", "secondCharacterThirdAction", "secondCharacterFourthAction", "secondCharacterFifthAction", "secondCharacterSixthAction", "secondCharacterSeventhAction", "secondCharacterEighthAction"]
    static let thirdCharacter = ["thirdCharacterFirstAction", "thirdCharacterSecondAction", "thirdCharacterThirdAction", "thirdCharacterFourthAction", "thirdCharacterFifthAction", "thirdCharacterSixthAction", "thirdCharacterSeventhAction", "thirdCharacterEighthAction"]
    static let fourthCharacter = ["fourthCharacterFirstAction", "fourthCharacterSecondAction", "fourthCharacterThirdAction", "fourthCharacterFourthAction", "fourthCharacterFifthAction", "fourthCharacterSixthAction", "fourthCharacterSeventhAction", "fourthCharacterEighthAction"]
}
