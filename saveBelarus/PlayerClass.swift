import Foundation

class PlayerClass: Codable {
    
    var name: String?
    var flags: Int?
    var data: String?
    
    init() {}

    private enum CodingKeys: String, CodingKey {
        case name
        case flags
        case data
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        name = try container.decodeIfPresent(String.self, forKey: .name)
        flags = try container.decodeIfPresent(Int.self, forKey: .flags)
        data = try container.decodeIfPresent(String.self, forKey: .data)
    }
    
    public func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(self.name, forKey: .name)
        try container.encode(self.flags, forKey: .flags)
        try container.encode(self.data, forKey: .data)
    }
}


