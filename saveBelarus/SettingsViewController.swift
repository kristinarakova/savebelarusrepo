import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var infoContainerView: UIView!
    @IBOutlet weak var buttonsContainerView: UIView!
    @IBOutlet weak var characterContainerView: UIView!
    
    @IBOutlet weak var underImageView: UIImageView!
    @IBOutlet weak var characterImageView: UIImageView!
    @IBOutlet weak var soundImageView: UIImageView!
    
    @IBOutlet weak var nameGameLabel: UILabel!
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var recordsButton: UIButton!
    @IBOutlet weak var soundButton: UIButton!
    @IBOutlet weak var changePlayerNameButton: UIButton!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    private var index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSelectedCharacterImageName()
        self.creatingButtonsUI()
        self.muteImageAdjustment()
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(leftSwipeDetected(_:)))
        leftSwipe.direction = .left
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(rightSwipeDetected(_:)))
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
    }
    
    @IBAction func startButton(_ sender: UIButton) {
        
        Manager.shared.saveCharacter(character: self.index)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func recordsButton(_ sender: UIButton) {
        
        Manager.shared.saveCharacter(character: self.index)
        guard let recordsController = storyboard?.instantiateViewController(withIdentifier: "RecordsViewController") as? RecordsViewController else {return}
        self.navigationController?.pushViewController(recordsController, animated: true)
    }
    
    @IBAction func changePlayerNameButton(_ sender: UIButton) {
        
        self.changePlayerNameAlert()
    }
    
    @IBAction func soundButton(_ sender: UIButton) {
        
        var sound = Manager.shared.loadMusicPlayer()
        if sound == true {
            sound = false
            self.soundImageView.image = UIImage(named: "soundOff")
            Manager.shared.saveMusicPlayer(sound: sound)
            Manager.shared.stopAudio()
        } else if sound == false {
            sound = true
            self.soundImageView.image = UIImage(named: "soundOn")
            Manager.shared.saveMusicPlayer(sound: sound)
            Manager.shared.playAudio()
        }
    }
    
    private func muteImageAdjustment () {
        
        let sound = Manager.shared.loadMusicPlayer()
        if sound == true {
            self.soundImageView.image = UIImage(named: "soundOn")
        } else {
            self.soundImageView.image = UIImage(named: "soundOff")
        }
    }
    
    private func addSelectedCharacterImageName () {
        
        if let character = Manager.shared.loadCharacter() {
            self.index = character
            self.characterImageView.image = Constants.charactersArray[self.index]
        }
        
        if let name = Manager.shared.loadName() {
            self.changePlayerNameButton.setTitle(name, for: .normal)
        }
    }
    
    private func creatingButtonsUI () {
        
        self.startButton.addGradientThreeColors(firstColor: .white, secondColor: .red, thirdColor: .white, opacity: 0.7, startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5), radius: self.startButton.frame.height/2)
        self.recordsButton
            .addGradientThreeColors(firstColor: .white, secondColor: .red, thirdColor: .white, opacity: 0.7, startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5), radius: self.recordsButton.frame.height/2)
        self.changePlayerNameButton
            .addGradientThreeColors(firstColor: .white, secondColor: .red, thirdColor: .white, opacity: 0.7, startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1.0, y: 0.5), radius: self.changePlayerNameButton.frame.height/2)
    }
    
    
    private func changePlayerNameAlert () {
        
        if let name = Manager.shared.loadName() {
            
            let alert = UIAlertController(title: "Change player name", message: "\(name)", preferredStyle: .alert)
            alert.addTextField { (textField) in
                textField.placeholder = "Enter a name..."
            }
            
            let commentAction = UIAlertAction(title: "Change", style: .default) { (_) in
                let textField = alert.textFields![0] as UITextField
                
                guard let newName = textField.text else {return}
                Manager.shared.saveName(name: newName)
                self.changePlayerNameButton.setTitle(newName, for: .normal)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (_) in
                // nothing
            }
            
            alert.addAction(commentAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func leftSwipeDetected (_ recognizer: UISwipeGestureRecognizer) {
        
        self.underImageView.frame.origin.x = self.characterContainerView.frame.width
        var nextPhoto = self.index + 1
        if nextPhoto > Constants.charactersArray.count - 1 {
            nextPhoto = 0
        }
        self.underImageView.image = Constants.charactersArray[nextPhoto]
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
            
            self.underImageView.frame = self.underImageView.frame.offsetBy(dx: -1 * self.underImageView.frame.size.width, dy: 0.0)
            
            self.characterImageView.frame = self.characterImageView.frame.offsetBy(dx: -1 * self.characterImageView.frame.size.width, dy: 0.0)
        }, completion: { (_) in
            
            self.underImageView.frame.origin.x = self.characterContainerView.frame.width
            self.underImageView.image = nil
            
            self.characterImageView.frame.origin.x = self.characterContainerView.frame.origin.x
            
            self.index += 1
            if self.index > Constants.charactersArray.count - 1  {
                self.index = 0
            }
            self.characterImageView.image = Constants.charactersArray[self.index]
        })
    }
    
    @IBAction func rightSwipeDetected (_ recognizer: UISwipeGestureRecognizer) {
        
        self.underImageView.frame.origin.x = -self.characterContainerView.frame.width
        var previousPhoto = self.index - 1
        if previousPhoto < 0 {
            previousPhoto = Constants.charactersArray.count - 1
        }
        self.underImageView.image = Constants.charactersArray[previousPhoto]
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
            
            self.underImageView.frame = self.underImageView.frame.offsetBy(dx: 1 * self.underImageView.frame.size.width, dy: 0.0)
            
            self.characterImageView.frame = self.characterImageView.frame.offsetBy(dx: 1 * self.characterContainerView.frame.size.width, dy: 0.0)
        }, completion: { (_) in
            
            self.underImageView.frame.origin.x = -self.characterContainerView.frame.width
            self.underImageView.image = nil
            
            self.characterImageView.frame.origin.x = self.characterContainerView.frame.origin.x
            
            self.index -= 1
            if self.index < 0 {
                self.index = Constants.charactersArray.count - 1
            }
            self.characterImageView.image = Constants.charactersArray[self.index]
        })
    }
}
