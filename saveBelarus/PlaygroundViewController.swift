import UIKit

class PlaygroundViewController: UIViewController {
    
    @IBOutlet weak var lowerPoliceContainerView: UIView!
    @IBOutlet weak var upperPoliceContainerView: UIView!
    @IBOutlet weak var menuBarContainerView: UIView!
    @IBOutlet weak var playgroundContainerView: UIView!
    @IBOutlet weak var roadMarkingsView: UIView!
    @IBOutlet weak var menuGradientView: UIView!
    
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var flagsCountLabel: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var activityIndicatorView: UIView!
    
    private var timer: Timer?
    
    private let characterImageView = UIImageView()
    private let flag = UIImageView()
    private let traffic = UIImageView()
    
    private var numberCharacterAction = 0
    private var flagCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.menuGradientView.addGradientThreeColors(firstColor: #colorLiteral(red: 0.800665915, green: 0.9136474133, blue: 0.9870060086, alpha: 1), secondColor: #colorLiteral(red: 0.800665915, green: 0.9136474133, blue: 0.9870060086, alpha: 1), thirdColor: .white, opacity: 1, startPoint: CGPoint(x: 0.5, y: 0), endPoint: CGPoint(x: 0.5, y: 1), radius: 0)
        self.playerNameLabel.text = Manager.shared.loadName()
        
        self.activityIndicator.style = .large
        self.activityIndicator.color = .white
        
        self.activityIndicator.startAnimating()
        self.activityIndicatorView.isHidden = false
        
        self.startGame()
    }
    
    private func startGame() {
        
        self.creatingCharacter()
        
        let moveUp = UISwipeGestureRecognizer(target: self, action: #selector(swipeDetected(_:)))
        moveUp.direction = .up
        let moveDown = UISwipeGestureRecognizer(target: self, action: #selector(swipeDetected(_:)))
        moveDown.direction = .down
        playgroundContainerView.addGestureRecognizer(moveUp)
        playgroundContainerView.addGestureRecognizer(moveDown)
        
        Timer.scheduledTimer(timeInterval: 0.25, target: self, selector: #selector(changeCharacter), userInfo: nil, repeats: true)
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(creatingRoadMarkings), userInfo: nil, repeats: true)
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(creatingLowerCurb), userInfo: nil, repeats: true)
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(creatingUpperCurb), userInfo: nil, repeats: true)
        
        Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(startAnimationGame), userInfo: nil, repeats: false)
    }
    
    @objc func startAnimationGame () {
        
        self.activityIndicator.stopAnimating()
        self.activityIndicatorView.isHidden = true
        
        Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(createFlags), userInfo: nil, repeats: true)
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(createTraffic), userInfo: nil, repeats: true)
        
        Timer.scheduledTimer(timeInterval: 0.001, target: self, selector: #selector(intersectsFlags), userInfo: nil, repeats: true)
        Timer.scheduledTimer(timeInterval: 0.001, target: self, selector: #selector(intersectsTraffic), userInfo: nil, repeats: true)
    }
    
    private func endGame () {
        
        let playerObject = PlayerClass()
        
        if let name = Manager.shared.loadName() {
            playerObject.name = name
            print()
        }
        playerObject.flags = flagCount
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM, HH:mm"
        let data = formatter.string(from: date)
        
        playerObject.data = data
        
        Manager.shared.save(object: playerObject)
        
        guard let gameOverController = storyboard?.instantiateViewController(withIdentifier: "GameOverViewController") as? GameOverViewController else {return}
        self.navigationController?.pushViewController(gameOverController, animated: true)
    }
    
    @IBAction func swipeDetected (_ recognizer: UISwipeGestureRecognizer) {
        
        if recognizer.direction == .up {
            let position = self.characterImageView.frame.origin.y
            self.characterImageView.frame.origin.y -= self.playgroundContainerView.frame.width/3
            if self.characterImageView.frame.origin.y < 0 {
                self.characterImageView.frame.origin.y = position
            }
        }
        if recognizer.direction == .down {
            let position = self.characterImageView.frame.origin.y
            self.characterImageView.frame.origin.y += self.playgroundContainerView.frame.width/3
            if self.characterImageView.frame.origin.y + self.characterImageView.frame.height > self.playgroundContainerView.frame.height {
                self.characterImageView.frame.origin.y = position
            }
        }
    }
    
    @objc func changeCharacter () {
        
        let character = Manager.shared.loadCharacter()
        switch character {
        case 0:
            characterImageView.image = UIImage(named: Constants.firstCharacter[self.numberCharacterAction])
        case 1:
            characterImageView.image = UIImage(named: Constants.secondCharacter[self.numberCharacterAction])
            self.numberCharacterAction += 1
        case 2:
            characterImageView.image = UIImage(named: Constants.thirdCharacter[self.numberCharacterAction])
            self.numberCharacterAction += 1
        case 3:
            characterImageView.image = UIImage(named: Constants.fourthCharacter[self.numberCharacterAction])
            self.numberCharacterAction += 1
        default:
            characterImageView.image = UIImage(named: Constants.firstCharacter[self.numberCharacterAction])
            self.numberCharacterAction += 1
        }
        
        self.numberCharacterAction += 1
        
        if self.numberCharacterAction == 8 {
            self.numberCharacterAction = 0
        }
    }
    
    private func creatingCharacter () {
        
        characterImageView.frame = CGRect(x: 30, y: playgroundContainerView.frame.height/3 + 5, width: playgroundContainerView.frame.height/4, height: playgroundContainerView.frame.height/3 - 10)
        self.playgroundContainerView.addSubview(characterImageView)
        
    }
    
    @objc func creatingRoadMarkings () {
        
        let upperMarkings = UIView()
        upperMarkings.frame = CGRect(
            x: self.roadMarkingsView.frame.width,
            y: self.roadMarkingsView.frame.height/3 - 5,
            width: 50, height: 10)
        upperMarkings.backgroundColor = .white
        upperMarkings.alpha = 1
        self.roadMarkingsView.addSubview(upperMarkings)
        
        let lowerMarkings = UIView()
        lowerMarkings.frame = CGRect(
            x: self.roadMarkingsView.frame.width,
            y: self.roadMarkingsView.frame.height/3 * 2 - 5,
            width: 50, height: 10)
        lowerMarkings.backgroundColor = .white
        lowerMarkings.alpha = 1
        self.roadMarkingsView.addSubview(lowerMarkings)
        
        UIView.animate(withDuration: 3, delay: 0, options: .curveLinear, animations: {
            upperMarkings.frame.origin.x = -self.roadMarkingsView.frame.width
            lowerMarkings.frame.origin.x = -self.roadMarkingsView.frame.width
        }, completion: { (_) in
            upperMarkings.removeFromSuperview()
            lowerMarkings.removeFromSuperview()
        })
    }
    
    @objc func creatingLowerCurb () {
        
        let lowerCordon = UIImageView()
        lowerCordon.frame = CGRect(x: self.lowerPoliceContainerView.frame.width,
                                   y: 0,
                                   width: self.lowerPoliceContainerView.frame.width,
                                   height: self.lowerPoliceContainerView.frame.height)
        lowerCordon.image = UIImage(named: "cordon")
        lowerPoliceContainerView.addSubview(lowerCordon)
        
        UIView.animate(withDuration: 3.0, delay: 0, options: .curveLinear, animations: {
            lowerCordon.frame.origin.x = -self.lowerPoliceContainerView.frame.width
        }, completion: { (_) in
            lowerCordon.removeFromSuperview()
        })
    }
    
    @objc func creatingUpperCurb () {
        
        let upperCordon = UIImageView()
        upperCordon.frame = CGRect(x: self.upperPoliceContainerView.frame.width,
                                   y: 0,
                                   width: self.upperPoliceContainerView.frame.width/2,
                                   height: self.upperPoliceContainerView.frame.height)
        upperCordon.image = UIImage(named: "cordon")
        upperPoliceContainerView.addSubview(upperCordon)
        
        UIView.animate(withDuration: 3.0, delay: 0, options: .curveLinear, animations: {
            upperCordon.frame.origin.x = -self.upperPoliceContainerView.frame.width/2
        }, completion: { (_) in
            upperCordon.removeFromSuperview()
        })
    }
    
    @objc func createFlags () {
        
        let one = self.playgroundContainerView.frame.height/6 - 15
        let two = self.playgroundContainerView.frame.height/2 - 15
        let three = self.playgroundContainerView.frame.height * 5 / 6 - 15
        
        let randomLineArrayY = [one, two, three]
        
        flag.frame = CGRect(
            x: self.playgroundContainerView.frame.width + 100,
            y: randomLineArrayY[Int.random(in: 0...2)],
            width: 60,
            height: 30)
        self.flag.image = UIImage(named: "wrwFlag")
        self.playgroundContainerView.addSubview(flag)
        
        UIView.animate(withDuration: 2.0, delay: 0, options: .curveLinear, animations: {
            self.flag.frame.origin.x = self.playgroundContainerView.frame.origin.x - 100
        }, completion: { (_) in
            self.flag.removeFromSuperview()
        })
    }
    
    @objc func createTraffic () {
        
        let policeCars = ["policeCar1", "policeCar2", "policeCar3"]
        
        let one = self.playgroundContainerView.frame.height/6 - playgroundContainerView.frame.width/10
        let two = self.playgroundContainerView.frame.height/2 - playgroundContainerView.frame.width/10
        let three = self.playgroundContainerView.frame.height * 5 / 6 - playgroundContainerView.frame.width/10
        
        let randomLineArrayY = [one, two, three]
        
        traffic.frame = CGRect(
            x: self.playgroundContainerView.frame.width + 100,
            y: randomLineArrayY[Int.random(in: 0...2)],
            width: playgroundContainerView.frame.width/2.5,
            height: playgroundContainerView.frame.width/5)
        self.traffic.image = UIImage(named: policeCars[Int.random(in: 0...2)])
        self.playgroundContainerView.addSubview(traffic)
        
        UIView.animate(withDuration: 2.0, delay: 0, options: .curveLinear, animations: {
            self.traffic.frame.origin.x = self.playgroundContainerView.frame.origin.x - self.playgroundContainerView.frame.width/2
        }, completion: { (_) in
            self.traffic.removeFromSuperview()
        })
        
    }
    
    @objc func intersectsFlags () {
        
        guard let characterPosition = characterImageView.layer.presentation()?.frame else {return}
        guard let flagPostition = flag.layer.presentation()?.frame else {return}
        if characterPosition.intersects(flagPostition) {
            self.flag.removeFromSuperview()
            self.flagCount += 1
            self.flagsCountLabel.text = String(self.flagCount)
        }
    }
    
    @objc func intersectsTraffic () {
        
        guard let characterPosition = characterImageView.layer.presentation()?.frame else {return}
        guard let trafficPosition = traffic.layer.presentation()?.frame else {return}
        if characterPosition.intersects(trafficPosition) {
            self.traffic.removeFromSuperview()
            self.endGame()
        }
    }
}
