import Foundation
import UIKit

extension UIView {
    
    func addGradientThreeColors(firstColor: UIColor, secondColor: UIColor, thirdColor: UIColor, opacity: Float, startPoint: CGPoint, endPoint: CGPoint, radius: CGFloat) {
        let gradient = CAGradientLayer()
        gradient.colors = [firstColor.cgColor, secondColor.cgColor, thirdColor.cgColor]
        gradient.opacity = opacity
        gradient.startPoint = startPoint
        gradient.endPoint = endPoint
        gradient.cornerRadius = radius
        gradient.frame = self.bounds
        self.layer.addSublayer(gradient)
    }
    
    func addRoundedShadow(cornerRadius: CGFloat, shadowColor: UIColor, offset: CGSize, shadowRadius: CGFloat, opacity: Float) {
        layer.cornerRadius = cornerRadius
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = offset
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = opacity
        layer.masksToBounds = false
    }
}

extension UIViewController {
    
    func resetResultsAlert (title: String, message: String, preferredStyle: UIAlertController.Style, titleFirstButton: String, styleFirstButton: UIAlertAction.Style, handlerFirst:  ((UIAlertAction) -> Void)?, titleSecondButton: String, styleSecondButton: UIAlertAction.Style, handlerSecond:  ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        let firstAction = UIAlertAction(title: titleFirstButton, style: styleFirstButton, handler: handlerFirst)
        let secondAction = UIAlertAction(title: titleSecondButton, style: styleSecondButton, handler: handlerSecond)
        alert.addAction(firstAction)
        alert.addAction(secondAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
           let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}
